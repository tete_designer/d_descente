var event = {
  all: 'mousedown touchstart pointerdown mouseenter mouseleave',
  type: '',
  start: '',
  enter: '',
  leave: '',
  move: '',
  end: ''
};

// イベント判定
event.check = function(e){
  var _default = e.type;
  event.start = _default;
  event.move = _default === 'pointerdown' ? 'pointermove' : (_default === 'touchstart' ? 'touchmove' : 'mousemove');
  event.end = _default === 'pointerdown' ? 'pointerup' : (_default === 'touchstart' ? 'touchend' : 'click');
  event.enter = _default === 'pointerenter' ? _default : (_default === 'touchenter' ? _default : 'mouseenter');
  event.leave = _default === 'pointerleave' ? _default : (_default === 'touchleave' ? _default : 'mouseleave');
};

event.on = function(type, elm, fn, delegate){
  $(document).off(event.all, elm).on(event.all, elm, function(e){
    e.preventDefault();
    event.check(e);
    if (fn != null) {
      if (e.type === event.enter || e.type === event.leave) {
        // mouseenter mouseleaveのとき、コールバック関数を即実行
        fn(this, e);
      } else {
        if (type === 'bind') {
          // jQuery $(element).on(event, func);
          $(elm).off(event.end).on(event.end, function(e){
            fn(this, e);
          });
        } else if (type === 'live') {
          // jQuery $(document).on(event, element, func);
          $(document).off(event.end, elm).on(event.end, elm, function(e){
            fn(this, e);
          });
        } else if (type === 'delegate') {
          // jQuery $(parentsElement).on(event, element, func);
          $(delegate).off(event.end, elm).on(event.end, elm, function(e){
            fn(this, e);
          });
        }
      }
    } else {
      // 引数にコールバック関数が定義されていないときの処理
    }
  });
};
// hushtag copy space
$(function(){
  $('#all_select').click(function(){
    selectDomElm(this);
  });
});

//$(function(){
//	var clickEventType = (( window.ontouchstart!==null ) ? 'click':'touchend');
// 
//    	$('#all_select').click(function(){
//    		selectDomElm(this);
//    	});
//    });
//$(document).on("click", "#all_select", function() {
//  selectDomElm(this);
//});
function selectDomElm(obj){
  var range = document.createRange();
  range.selectNodeContents(obj);
  var selection = window.getSelection();
  selection.removeAllRanges();
  selection.addRange(range);
}

